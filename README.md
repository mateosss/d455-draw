Simple example that lets you draw based on proximity.

# Compile and run

```
c++ draw.cpp -lrealsense2 `pkg-config opencv --libs --cflags`
./a.out
```

[![](http://img.youtube.com/vi/3sCkLeMhQMk/0.jpg)](http://www.youtube.com/watch?v=3sCkLeMhQMk "Demo of the example")
