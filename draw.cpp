// Copyright 2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>

int main(int argc, char* argv[]) try {
  using namespace cv;

  Mat drawing;
  bool drawing_initialized = false;
  rs2::colorizer color_map;
  rs2::pipeline pipe;
  pipe.start();

  const auto window_name = "Display Image";
  namedWindow(window_name, WINDOW_AUTOSIZE);

  while (waitKey(1) < 0 &&
         getWindowProperty(window_name, WND_PROP_AUTOSIZE) >= 0) {
    rs2::frameset data = pipe.wait_for_frames();
    rs2::depth_frame depth = data.get_depth_frame();
    rs2::frame color_frame = depth.apply_filter(color_map);

    const int w = depth.as<rs2::video_frame>().get_width();
    const int h = depth.as<rs2::video_frame>().get_height();

    if (!drawing_initialized) {
      drawing = Mat{h, w, CV_8UC3, Scalar(100, 0, 0)};
      drawing_initialized = true;
    }

    Mat image(Size(w, h), CV_8UC3, (void*)color_frame.get_data());
    cvtColor(image, image, COLOR_BGR2GRAY);
    cvtColor(image, image, COLOR_GRAY2BGR);

    // Erase when "touching" the bottom left of the display
    bool erase = depth.get_distance(w - 10, h - 10) < 0.6f;
    if (erase)
      for (size_t x = 0; x < w; x++)
        for (size_t y = 0; y < h; y++)
          drawing.at<Vec3b>(y, x) = Vec3b(40, 0, 0);
    else
      for (size_t x = 0; x < w; x++)
        for (size_t y = 0; y < h; y++) {
          float d = depth.get_distance(x, y);
          if (d > 0.5f && d < 0.6f)
            drawing.at<Vec3b>(y, x) = Vec3b(100, 0, 100);
        }

    image += drawing;
    Mat flipped;  // Auxiliary matrix as flipping can't be done in place
    flip(image, flipped, 1);
    imshow(window_name, flipped);
  }

  return EXIT_SUCCESS;
} catch (const rs2::error& e) {
  std::cerr << "RealSense error calling " << e.get_failed_function() << "("
            << e.get_failed_args() << "):\n    " << e.what() << std::endl;
  return EXIT_FAILURE;
} catch (const std::exception& e) {
  std::cerr << e.what() << std::endl;
  return EXIT_FAILURE;
}
